/* SystemJS module definition */
declare var module: NodeModule;
declare module 'three';
declare module 'physijs';
declare module 'whs';

interface NodeModule {
  id: string;
}
