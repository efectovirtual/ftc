import { Component, OnInit,  HostListener, ElementRef, ViewChild } from '@angular/core';
import { Post } from '../posts/post';
import { PostsService } from '../posts/posts.service';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { OwlCarousel } from 'ngx-owl-carousel';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
  providers: [PostsService]
})

export class HomepageComponent implements OnInit {
  @ViewChild('owlElement') owlElement: OwlCarousel;
  slides: Post[];
  first_moment = true;
  error: string;

  @HostListener('document:click', ['$event'])
  clickout(event) {
      if (this.first_moment){
        let itera = 1;
        let ruleta = setInterval(() => {
          if( itera < Math.random() * (35 - 25) + 25 ){
            this.owlElement.next([100]);
            itera++;
          }else{
            clearInterval( ruleta );
          }
        }, 101);
        this.first_moment = false;
      }
  }

  constructor( private eRef: ElementRef, private app: AppComponent, private postsService: PostsService, private router: Router ) { }

  getSlides(){
    this.postsService
      .getSlides()
      .subscribe(res => {
        // success
        this.slides = res;
      }, err => {
        // error
        this.error = err;
      });

  }

  ngOnInit() {
    this.app.cssClass = 'homepage';
  	this.getSlides();
  }

}
