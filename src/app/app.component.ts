import { Component, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'Flipping The Coin';
  menu_open = false;
  @HostBinding('class') public cssClass;
  ngOnInit() {
    this.cssClass = 'homepage';
  }
}
