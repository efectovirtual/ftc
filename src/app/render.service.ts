import { Component, ElementRef, Directive, HostListener, Injectable, Input, OnInit } from '@angular/core';

declare var THREE: any;
declare var Physijs: any;


@Component({
	selector: 'three-renderer',
	template: ''
})

export class RenderService implements OnInit {
	i:number;
	loader = new THREE.TextureLoader();
	scene = new Physijs.Scene;
	camera = THREE.PerspectiveCamera;
	geometry = new THREE.CylinderGeometry( 0.52, 0.52, 0.048, 185 );
    material = new THREE.MeshBasicMaterial( { color: 0xffffff, wireframe: false }, 10, 0.7 );
    renderer = new THREE.WebGLRenderer( { alpha: true } );
    isPaused = true;
    light = new THREE.AmbientLight( 0x404040, 5 );

    coin_cap_geo:any;
    coin_cap_texture:any;
    coin_cap_texture_back:any;
    coin_sides:any;
    coin_cap_mat:any;
    coin_cap_mat_back:any;
    coin_cap_top:any;
	  coin_cap_bottom:any;

    /* NEW */

    /* NEW */

    ground_material = Physijs.createMaterial(
		new THREE.MeshLambertMaterial({color: 0x0000ff, transparent: true, opacity: 0}),
		1, // high friction
		0.5 // low restitution
	);

	mesh = new Physijs.CylinderMesh( this.geometry, this.material, 10 );

	ground = new Physijs.BoxMesh(
		new THREE.BoxGeometry(10000, 1, 10000),
		this.ground_material,
		0 // mass
	);

	wall1 = new Physijs.BoxMesh(
		new THREE.BoxGeometry(1, 100000, 100000),
		this.ground_material,
		0 // mass
	);

	wall2 = new Physijs.BoxMesh(
		new THREE.BoxGeometry(1, 100000, 100000),
		this.ground_material,
		0 // mass
	);

	wall3 = new Physijs.BoxMesh(
		new THREE.BoxGeometry(100000, 100000, 1),
		this.ground_material,
		0 // mass
	);

	wall4 = new Physijs.BoxMesh(
		new THREE.BoxGeometry(100000, 100000, 1),
		this.ground_material,
		0 // mass
	);

	viewAngle: number = 75;
	near: number = 0.1;
	far: number = 10000;
	mobHeight: any;
	mobWidth: any;

	get aspect(): number {
        return window.innerWidth / window.innerHeight;
    }

  @HostListener('document:click', ['$event'])
  clickout(event) {
      this.flip()
  }

	@HostListener('window:resize', ['$event'])
	onResize(event) {
	  //console.log(event.target.innerWidth);
		this.renderer.setSize( event.target.innerWidth, event.target.innerHeight );
		this.camera.aspect = event.target.innerWidth / event.target.innerHeight;
    this.camera.updateProjectionMatrix();
	}

  constructor( private eRef: ElementRef ) {



  }

  ngOnInit(){

  	this.scene = new Physijs.Scene;
	this.mobHeight = window.innerHeight;
	this.mobWidth = window.innerWidth;

    this.scene.setGravity( new THREE.Vector3(0, -10, 0) );
    this.camera = new THREE.PerspectiveCamera(
					20,
					this.aspect,
					1,
					150
				);

	this.camera.position.set(
		0,
		22,
		25
	);

	this.camera.rotation.set(
		-0.4,
		0,
		0
	);

	// this.camera.lookAt( new THREE.Vector3(0, 0, 10) );

	this.renderer.setSize( this.mobWidth, this.mobHeight );
	this.renderer.setClearColor( 0x000000, 0 );

	this.coin_cap_geo = new THREE.Geometry();
	let r = 0.48;
	for ( let i=0; i<100; i++) {
	  let a = i * 1/100 * Math.PI * 2;
	  let z = Math.sin(a);
	  let x = Math.cos(a);
	  let a1 = (i+1) * 1/100 * Math.PI * 2;
	  let z1 = Math.sin(a1);
	  let x1 = Math.cos(a1);
	  this.coin_cap_geo.vertices.push(
	    new THREE.Vector3(0, 0, 0),
	    new THREE.Vector3(x*r, 0, z*r),
	    new THREE.Vector3(x1*r, 0, z1*r)
	  );
	  this.coin_cap_geo.faceVertexUvs[0].push([
	    new THREE.Vector2(0.5, 0.5),
	    new THREE.Vector2(x/2+0.5, z/2+0.5),
	    new THREE.Vector2(x1/2+0.5, z1/2+0.5)
	  ]);
	  this.coin_cap_geo.faces.push(new THREE.Face3(i*3, i*3+1, i*3+2));
	}
	// this.coin_cap_geo.computeCentroids();
	this.coin_cap_geo.computeFaceNormals();

	// load a resource
	this.loader.load(
		// resource URL
		'./assets/img/FTC_coin_head.png',
		// Function when resource is loaded
		texture => {
			// do something with the texture
			this.coin_cap_mat = new THREE.MeshLambertMaterial({map: texture});
			this.coin_cap_top = new Physijs.BoxMesh( this.coin_cap_geo, this.coin_cap_mat );
			this.coin_cap_top.position.y = 0.025;
			this.coin_cap_top.rotation.x = Math.PI;
			this.mesh.add(this.coin_cap_top);
		},
		// Function called when download progresses
		 xhr => {
			console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
		},
		// Function called when download errors
		 xhr => {
			console.log( 'An error happened' );
		}
	);

	// load a resource
	this.loader.load(
		// resource URL
		'./assets/img/FTC_coin_tail.png',
		// Function when resource is loaded
		texture => {
			// do something with the texture
			this.coin_cap_mat_back = new THREE.MeshLambertMaterial({map: texture});
			this.coin_cap_bottom = new Physijs.BoxMesh( this.coin_cap_geo, this.coin_cap_mat_back );
			this.coin_cap_bottom.position.y = -0.025;
			this.mesh.add(this.coin_cap_bottom);
			this.ground.receiveShadow = true;
		    this.scene.add( this.ground );
		    // this.scene.add( this.coin );
		    // this.coin.position.set( 0, 7, 9 );
		    // this.coin.rotation.x = Math.PI / 2;

		    this.wall1.position.set(-8, 0, 0);
		    this.wall2.position.set(8, 0, 0);
		    this.wall3.position.set(0, 0, -40);
		    this.wall4.position.set(0, 2.5, -4);
		    this.wall4.rotation.x = ( Math.PI / 3 );
			this.mesh.position.set( 0, 12.2, 3.5 );
			this.mesh.rotation.x = ( Math.PI / 2 ) - 0.4;
		    this.ground.position.set(-200, -200, -200);


		    this.scene.add( this.wall1 );
		    this.scene.add( this.wall2 );
		    this.scene.add( this.wall3 );
		    this.scene.add( this.wall4 );
			this.scene.add( this.mesh );
			this.scene.add( this.light );
		},
		// Function called when download progresses
		 xhr => {
			console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
		},
		// Function called when download errors
		 xhr => {
			console.log( 'An error happened' );
		}
	);

    document.body.appendChild( this.renderer.domElement );
  	this.animate();
  }

  animate() {
  	// console.log("animating");
  	if (!this.isPaused) {
  		this.scene.simulate();
  	}else{
  		this.mesh.rotation.z += 0.01;
  	}

    requestAnimationFrame(() => this.animate());

    this.renderer.render( this.scene, this.camera );
  }

  flip() {
  	if( this.isPaused ){
		this.mesh.setLinearVelocity(new THREE.Vector3(0.5, 8, -7.5));
		  this.renderer.setClearColor( 0x000000, 1 );
    	this.mesh.setAngularVelocity(new THREE.Vector3(155, -35, 3));
    	this.isPaused = false;
  	}else{
	  	let randomX = Math.floor(Math.random() * 26) - 13;
	  	let randomY = Math.floor(Math.random() * 10) + 1;
	  	let randomZ = Math.floor(Math.random() * 10) - 4;

	  	let randomXr = Math.floor(Math.random() * 250) - 125;
	  	let randomYr = Math.floor(Math.random() * 28) - 14;
	  	let randomZr = Math.floor(Math.random() * 40) - 20;

	  	this.mesh.setLinearVelocity(new THREE.Vector3(randomX, randomY, randomZ));
	    this.mesh.setAngularVelocity(new THREE.Vector3(randomXr, randomYr, randomZr));
  	}
  }
}
