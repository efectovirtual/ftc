import { Component, Input, Output, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../post';
import { PostsService } from '../posts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppComponent } from '../../app.component';
@Component({
  selector: 'app-post-single',
  templateUrl: './post-single.component.html',
  styleUrls: ['./post-single.component.css'],
  providers: [PostsService]
})
export class PostSingleComponent implements OnInit {

  error: any;
  post_slug: any;
  private sub: any;
  post: Post;

  constructor(
    private postsService: PostsService,
    private route: ActivatedRoute,
    private router: Router,
    private app: AppComponent
  ) {

  }

  getPost(slug){
    this.postsService
      .getPost(slug)
      .subscribe( (res) => {
        // success
        this.post = res[0];
        this.app.cssClass = 'archive single';
      }, (err) => {
        // error
        this.error = err;
      });
  }

  ngOnInit() {
  	this.sub = this.route.params.subscribe(params => {
  		this.post_slug = params['post'];
  		if( this.post_slug ){
  			this.getPost( this.post_slug );
  		}
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
