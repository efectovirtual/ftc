import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Post } from '../posts/post';
import { PageService } from './page.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  providers: [PageService]
})

export class PageComponent implements OnInit {

  private sub: any;
  page: Post;
  slug: any;
  error: any;

  constructor( private app: AppComponent, private pageService: PageService, private route: ActivatedRoute ) { }

  getPage(slug){
    this.pageService
      .getPage(slug)
      .subscribe( (res) => {
        // success
        this.page = res[0];
        console.log( "Page: ", this.page );
      }, (err) => {
        // error
        this.error = err;
      });
  }

  ngOnInit() {
  	this.app.cssClass = 'page';
  	this.sub = this.route.params.subscribe(params => {
  		this.slug = params['slug']; 
  		if( this.slug ){
  			this.getPage( this.slug );
  		}
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
