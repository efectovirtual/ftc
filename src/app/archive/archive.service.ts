import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Post } from '../posts/post';

import { environment } from '../../environments/environment';

@Injectable()
export class ArchiveService {

	private _wpBase = environment.wpBase;
    constructor(private http: Http) { }

    getCategory(slug): Observable<Post> {

      return this.http
        .get(this._wpBase + `categories?_embed&slug=${slug}`)
        .map((res: Response) => res.json())
        .catch((err: Response | any) => {
          console.error(err)
          return Observable.throw(err);
        });
    }

    getPosts(id): Observable<Post[]> {

      return this.http
        .get(this._wpBase + `posts?_embed&categories=${id}`)
        .map((res: Response) => res.json())
        .catch((err: Response | any) => {
          console.error(err)
          return Observable.throw(err);
        });
  	}
}
