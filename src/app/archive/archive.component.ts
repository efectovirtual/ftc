import { Component, OnInit , OnDestroy, ChangeDetectorRef, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { Post } from '../posts/post';
import { ArchiveService } from './archive.service';
import { PostsService } from '../posts/posts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.scss'],
  providers: [ArchiveService, PostsService]
})
export class ArchiveComponent implements OnInit {
  category: any;
  category_id: number;
  private sub: any;
  slug: any;
  post_slug: any;
  post: Post;
  posts: Post[];
  error: string;
  width: any ='';
  new: string;

  constructor(
    private change: ChangeDetectorRef,
    private app: AppComponent,
  	private route: ActivatedRoute,
  	private archiveService: ArchiveService,
  	private location: Location,
  	private postsService: PostsService,
  	private router: Router,
    private _ngZone: NgZone
  ) {

  }

  getCategory( slug ){
    this.category = null;
    this.post = null;
    this.posts = null;
    this.archiveService
      .getCategory( slug )
      .subscribe(res => {
        // success
        this.category = res[0];
        this.category_id = this.category.id;
        this.getPosts( this.category_id );
      }, err => {
        // error
        this.error = err;
      });
  }

  changeClass( nueva){
    this.app.cssClass = nueva;
  }

  getBG( url ){
    if( url._embedded != undefined && url._embedded['wp:featuredmedia'] != undefined ){
      return url._embedded['wp:featuredmedia']['0'].media_details.sizes.medium.source_url;
    } else {
      return  'http://placehold.it/420x260?text=image+not+assigned+yet';
    }
  }

  getPosts( id ){
    this.archiveService
      .getPosts( id )
      .subscribe(res => {
          // success
          this.posts = res;
          console.log( "posts", this.posts );
      		if( this.post_slug ){
            // this.app.cssClass = 'archive single';
      		}else{
            // this.app.cssClass = 'archive';
          }
      }, err => {
        // error
        this.error = err;
      });
  }

  ngOnInit() {
  	this.sub = this.route.params.subscribe(params => {
    		this.slug = params['slug'];
    		this.getCategory( this.slug );
        this.post_slug = params['post'];
    		if( this.post_slug ){
          // this.app.cssClass = 'archive single';
    		}else{
          this.app.cssClass = 'archive';
        }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
