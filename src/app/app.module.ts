import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { OwlModule } from 'ngx-owl-carousel';

import { AppComponent } from './app.component';
import { PostListComponent } from './posts/post-list/post-list.component';
import { Wpng2RoutingModule } from './app-routing.module';
import { PostSingleComponent } from './posts/post-single/post-single.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ArchiveComponent } from './archive/archive.component';
import { PageComponent } from './page/page.component';

import { StateService } from './state.service';
import { RenderService } from './render.service';

import { EscapeHtmlPipe } from './keep-html.pipe';

@NgModule({
  declarations: [
    AppComponent,
    PostListComponent,
    PostSingleComponent,
    HomepageComponent,
    ArchiveComponent,
    PageComponent,
    RenderService,
    EscapeHtmlPipe
  ],
  imports: [
    BrowserModule,
    OwlModule,
    FormsModule,
    HttpModule,
    Wpng2RoutingModule
  ],
  providers: [ StateService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
